# Configures Client
# This script installs Ansible
# Then, using Ansible, Installs Terraform, and Vault
# Prereq :  Ubuntu, RHEL, CentOS, WSL2
#
# Can be run from Client directly like so:
# bash <(curl -Ls https://gitlab.com/israel-morales/XJ9/-/blob/master/configure-client.sh)

if [ -n "$(uname -a | grep -e Ubuntu -e microsoft-standard)" ]; then
 
#Installs Ansible,Git on Ubuntu Distros
sudo apt update
sudo apt install software-properties-common -y
sudo apt-add-repository -y --update ppa:ansible/ansible
sudo apt install ansible -y
sudo apt install git -y

elif [ -n "$(uname -a | grep -e rhel -e centos)" ]; then

#Installs Ansible,Git on RHEL and CENTOS Distros
sudo yum install epel-release -y
sudo yum install ansible -y
sudo yum install git -y

else
echo "this OS is not supported"
exit 1

fi 

#Clones this project and runs the Playbooks for installing Terraform and Vault
git clone https://gitlab.com/israel-morales/XJ9 
ansible-playbook --connection=local --inventory 127.0.0.1, XJ9/ansible-roles/terraform.yml
ansible-playbook --connection=local --inventory 127.0.0.1, XJ9/ansible-roles/vault.yml
rm -rf XJ9/
